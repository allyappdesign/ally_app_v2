package com.stevemuho.ally;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyAccount extends AppCompatActivity {

    @BindView(R.id.iv_profile_image)
    AvatarView avatarView;
    IImageLoader imageLoader;
    @BindView(R.id.userProfileChange)
    AppCompatImageView userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Account Settings");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageLoader = new GlideLoader();

        imageLoader.loadImage(avatarView,"","Muhoro");

    }

    @OnClick(R.id.userProfileChange)
    public void changeUserPhoto(){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_my_account) {

            startActivity( new Intent(this,MyAccount.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
