package com.stevemuho.ally;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.stevemuho.ally.utils.ToolbarUtility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_)
    Toolbar toolbar;
    @BindView(R.id.rvToolBar)
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        ButterKnife.bind(this);
        ToolbarUtility.setUpToolbar(this, toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ToolbarUtility.setUpToolbar(this, toolbar,null,"Muhoro","2.30 pm");

    }

    //open users profile screen
    @OnClick(R.id.rvToolBar)
    public void openUserProfile(){
        //send the users name to the next activity
        Intent intent = new Intent(this,UserProfile.class);
        intent.putExtra("userName","Muhoro");
        startActivity( intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chats, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
