package com.stevemuho.ally;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignIn extends AppCompatActivity {

    //get all reference to all views
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.txtCreateAccount)
    TextView txtCreateAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //this code underlines the textview
        txtCreateAccount.setPaintFlags(txtCreateAccount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


    }

    //takes you to sign up page
    @OnClick(R.id.txtCreateAccount) void goToSignUp(){

        startActivity( new Intent(this,SignUpActivity.class));

    }

    //signs you in
    @OnClick(R.id.btnSignIn)
    public void signInUser(){

        startActivity( new Intent(this,MainActivity.class));
    }

}
