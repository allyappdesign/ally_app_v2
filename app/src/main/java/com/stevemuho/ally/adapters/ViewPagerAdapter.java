package com.stevemuho.ally.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.stevemuho.ally.fragments.ChatsFragments;
import com.stevemuho.ally.fragments.ContactsFragment;

/**
 * Created by muho on 1/10/18.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private static int TAB_COUNT = 2;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return ChatsFragments.newInstance();
            case 1:
                return ContactsFragment.newInstance();
        }

        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ChatsFragments.TITLE;

            case 1:
                return ContactsFragment.TITLE;
        }
        return super.getPageTitle(position);
    }
}
