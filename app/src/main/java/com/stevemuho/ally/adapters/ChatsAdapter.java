package com.stevemuho.ally.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stevemuho.ally.ChatsActivity;
import com.stevemuho.ally.R;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by muho on 1/10/18.
 */

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.MyViewHolder> {

   private IImageLoader imageLoader;

    public ChatsAdapter(Context context){

        imageLoader = new GlideLoader();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ChatsAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chat_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //replace your code here
        holder.txtUreadMessages.setText(String.valueOf(5));
        holder.txtUserChatTime.setText("9.50 pm");
        holder.txtUserChatName.setText("Steve Muhoro");
        holder.txtUserChatBio.setText("Last message sent");
        imageLoader.loadImage(holder.imageChatUser, "", "Muhoro");

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txtUserChatName)
        TextView txtUserChatName;
        @BindView(R.id.txtUserChatBio)
        TextView txtUserChatBio;
        @BindView(R.id.txtUserChatTime)
        TextView txtUserChatTime;
        @BindView(R.id.txtUnreadMessages)
        TextView txtUreadMessages;
        @BindView(R.id.imageChatUser)
        AvatarView imageChatUser;
        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            view.getContext().startActivity( new Intent(view.getContext(),ChatsActivity.class));
        }
    }
}
