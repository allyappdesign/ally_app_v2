package com.stevemuho.ally.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stevemuho.ally.R;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by muho on 1/11/18.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {

    private IImageLoader imageLoader;

    public ContactsAdapter(Context context){

        imageLoader = new GlideLoader();
    }
    @Override
    public ContactsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ContactsAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_contacts, parent, false));

    }

    @Override
    public void onBindViewHolder(ContactsAdapter.MyViewHolder holder, int position) {


        //replace your code here
        holder.txtUserChatName.setText("Philip Mudeyo");
        holder.txtUserChatBio.setText("User Status comes here");
        imageLoader.loadImage(holder.imageChatUser, "", "Philip");


    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtUserChatName)
        TextView txtUserChatName;
        @BindView(R.id.txtUserChatBio)
        TextView txtUserChatBio;
        @BindView(R.id.imageChatUser)
        AvatarView imageChatUser;
        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }
}
