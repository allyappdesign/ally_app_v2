package com.stevemuho.ally.utils;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.stevemuho.ally.R;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.views.AvatarView;
import agency.tango.android.avatarviewglide.GlideLoader;


/**
 * Created by makun on 10-Aug-17.
 */

public class ToolbarUtility {
    public static void setUpToolbar(AppCompatActivity activity, Toolbar toolbar) {
        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ActivityInfo activityInfo = null;
            try {
                activityInfo = activity.getPackageManager().getActivityInfo(
                        activity.getComponentName(), PackageManager.GET_META_DATA);
                TextView tvToolBarTitle = (TextView) activity.findViewById(R.id.tvToolBarTitle);
                tvToolBarTitle.setText(activityInfo.loadLabel(activity.getPackageManager())
                        .toString());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setUpToolbar(AppCompatActivity activity, Toolbar toolbar,String avatarUrl,String userName,String online) {
        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView tvToolBarTitle = (TextView) activity.findViewById(R.id.tvToolBarTitle);
            tvToolBarTitle.setText(userName);

            //setting the users status online or
            TextView txtOnlineStatus = activity.findViewById(R.id.txtOnlineStatus);
            txtOnlineStatus.setText(online);

            //sets the users avatar
            AvatarView avatarView = activity.findViewById(R.id.avatarUser);
            IImageLoader imageLoader = new GlideLoader();

            if(avatarUrl==null){

                imageLoader.loadImage(avatarView, "", userName);
            }
            imageLoader.loadImage(avatarView, avatarUrl, userName);


        }
    }
}
