package com.stevemuho.ally.utils;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by muho on 9/25/17.
 */

public class EqualWidthHeightTextView extends android.support.v7.widget.AppCompatTextView {

    public EqualWidthHeightTextView(Context context) {
        super(context);
    }

    public EqualWidthHeightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EqualWidthHeightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int h = this.getMeasuredHeight();
        int w = this.getMeasuredWidth();
        int r = Math.max(w,h);

        setMeasuredDimension(r, r);

    }
}
